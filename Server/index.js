import mysql from "mysql"
import express from "express"
import cors from 'cors'

const app = express()

const db = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "testcrud",
})

app.use(express.json())
app.use(cors())

app.get("/", (req, res) => {
    res.json("hey !!!!")
})
app.get("/books", (req, res) => {
    const q = "SELECT * FROM books"
    db.query(q, (err, data) => {
        if (err) return res.json(err)
        return res.json(data)
    })
})
app.post("/books", (req, res) => {
    const q = "INSERT INTO books (`title`,`desc`,`cover`,`price`) VALUE(?)";
    const values = [
        req.body.title,
        req.body.desc,
        req.body.cover,
        req.body.price,
    ]
    db.query(q, [values], (err, data) => {
        if (err) return res.json(err)
        return res.json("hey!!")
    })
})

app.delete("/books/:id", (req, res) => {
    const bookId = req.params.id;
    const q = "DELETE FROM books WHERE id = ?";

    db.query(q, [bookId], (err, data) => {
        if (err) return res.json(err)
        return res.json("hey deleted!!")
    })
})
app.put("/books/:id", (req, res) => {
    const bookId = req.params.id;
    const q = "UPDATE books SET `title`= ?,`desc`= ?,`cover`= ?,`price`= ? Where id=?";
    const values = [
        req.body.title,
        req.body.desc,
        req.body.cover,
        req.body.price,
    ]
    db.query(q, [...values ,bookId], (err, data) => {
        if (err) return res.json(err)
        return res.json("hey update!!")
    })
})

app.listen(5000, () => {
    console.log("server started on port 5000");
})