import { BrowserRouter, Route, Routes, Link } from 'react-router-dom';
import React from 'react';
import CreateUser from './components/CreateUser';
import ListUser from './components/ListUser';
import DeleteUser from './components/DeleteUser';
import EditUser from './components/EditUser';
import Navbar from './components/Navbar/Navbar';
import './App.css';


function App() {
  return (
    <div className='App'>
      <Navbar/>
      <Routes>
        <Route path="/" element={<ListUser />} />
        <Route path="/create" element={<CreateUser />} />
        <Route path="/delete" element={<DeleteUser />} />                 
        <Route path="/edit/:id" element={<EditUser />} />
      </Routes>
    </div>
  );
}

export default App;
