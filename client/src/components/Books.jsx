import React from 'react'
import { useEffect } from 'react'
import { useState } from 'react'
import axios from 'axios'

export const Books = () => {

  const [books,setBooks]= useState([])

  useEffect(()=>{
    const fetchAllBooks = async ()=>{
      try {
        const res = await axios.get("http://localhost:5000/books")
        setBooks(res.data)
      } catch (error) {
        console.log(error);
      }
    }
    fetchAllBooks()
  },[])
  return (
    <div>
      <h1>Books list</h1>
  {books.map(book=>(
<div className='book' key={book.id} >
{book.cover && <img src={book.cover} alt=""/>}
<h2>{book.title}</h2>
<p>{book.desc}</p>
<span> {book.price} </span>
</div>
  ))}

    </div>
  )
}
