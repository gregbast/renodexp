import React, { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

export default function CreateUser() {
const[inputs, setInputs] = useState({
    name:"",
    email:"",
    
})
const navigate = useNavigate()
const handleChange = (e)=> {
    setInputs((prev) => ({...prev , [e.target.name]:e.target.value}));
// const name = event.target.name;
// const value = event.target.value;
}
console.log(inputs);

const handleClick = async (e)=>{
    e.preventDefault();
    try {
        axios.post('http://127.0.0.1:8000/api/users', inputs)
        navigate("/")
    } catch (error) {
        console.log(error);
        
    }
}




    return (
        <div className='form'>
        <h1>Create User</h1>
           
                <label>name : </label>
                <input type="text" name="name" onChange={handleChange}/>
                <label>email : </label>
                <input type="text" name="email" onChange={handleChange}/>
                {/* <label>cover : </label>
                <input type="text" name="cover" onChange={handleChange} />
                <label>price : </label>
                <input type="number" name="price" onChange={handleChange} /> */}
                <button type="submit" onClick={handleClick}>Save</button>
          
        </div>
    )
}
