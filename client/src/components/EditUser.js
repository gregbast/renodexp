import React, { useState } from 'react';
import axios from 'axios';
import { useLocation, useNavigate } from 'react-router-dom';

export default function EditUser() {
const[inputs, setInputs] = useState({
    title:"",
    desc:"",
    cover:"",
    price:null,
})
const navigate = useNavigate()
const location = useLocation()

const bookId = location.pathname.split("/")[2]

const handleChange = (e)=> {
    setInputs((prev) => ({...prev , [e.target.name]:e.target.value}));
// const name = event.target.name;
// const value = event.target.value;
}
console.log(inputs);

const handleClick = async e =>{
    e.preventDefault();
    try {
      await  axios.put('http://localhost:5000/books/'+ bookId, inputs)
        navigate("/")
    } catch (error) {
        console.log(error);
        
    }
}




    return (
        <div className='form'>
        <h1>update User</h1>
           
                <label>title : </label>
                <input type="text" name="title" onChange={handleChange}/>
                <label>desc : </label>
                <input type="text" name="desc" onChange={handleChange}/>
                <label>cover : </label>
                <input type="text" name="cover" onChange={handleChange} />
                <label>price : </label>
                <input type="number" name="price" onChange={handleChange} />
                <button type="submit" onClick={handleClick}>update</button>
          
        </div>
    )
}
