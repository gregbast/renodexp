import React from 'react'
import { useEffect } from 'react'
import { useState } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'

export default function ListUser()  {

  const [books,setBooks]= useState({})

  useEffect(()=>{
    const fetchAllBooks = async (data)=>{
      try {
        const res = await axios.get("http://localhost:8000/api/users")
        setBooks(res.data)
      } catch (error) {
        console.log(error);
      }
    }
    fetchAllBooks()
  },[])

// const handleDelete = async (id)=>{
//   try {
//     await axios.delete("http://localhost:5000/books/" +id)
//     window.location.reload()
//   } catch (error) {
//     console.log(error);
//   }
// }

  return (
    <div>
      <h1>Books list</h1>
  {books.map(book=>(
<div className='book' key={book.id} >

<h2>{book.name}</h2>
{/* <p>{book.desc}</p>
<span> {book.price} </span>
<button className='delete' onClick={()=>handleDelete(book.id)}>Delete</button>
<button className='update'> <Link to={`/edit/${book.id}`} >update</Link> </button> */}
</div>
  ))}

    </div>
  )
}
