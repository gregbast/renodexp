import React from 'react';
import {Link} from 'react-router-dom';

export default function Navbar() {
  return (
    <nav>
        <Link to='/'>Accueil</Link>
        <Link to='/create'>Create</Link>
        <Link to='/edit'>Edit</Link>
        <Link to='/delete'>Delete</Link>
    </nav>
  )
}
